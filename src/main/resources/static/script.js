var svg = d3.select("#graph"),
    width = +svg.attr("width"),
    height = +svg.attr("height");

var color = d3.scaleOrdinal(d3.schemeCategory20);

var simulation = d3.forceSimulation()
    .force("link", d3.forceLink().id(function(d) { return d.id; }))
    .force("charge", d3.forceManyBody())

var test;
var tmp;

var loadGraph = function(url) {

 d3.json(url, function(error, graph) {
    d3.selectAll("svg > *").remove();
 console.log("Reload graph")
   if (error) throw error;

   var link = svg.append("g")
       .attr("class", "links")
     .selectAll("line")
     .data(graph.links)
   .attr("class", function(d) { return color(d.group); })
     .enter().append("line")
       .attr("stroke-width", function(d) { return Math.sqrt(d.value); });

   var node = svg.append("g")
       .attr("class", "nodes")
     .selectAll("g")
     .data(graph.nodes)
     .enter().append("g")

   var circles = node.append("circle")
       .attr("r", 5)
       .attr("fill", function(d) { return color(d.group); })
       .call(d3.drag()
           .on("start", dragstarted)
           .on("drag", dragged)
           .on("end", dragended));

   var lables = node.append("text")
       .text(function(d) {
         return d.id;
       })
       .attr('x', 6)
       .attr('y', 3);

   node.append("title")
       .text(function(d) { return d.id; });



   simulation
       .nodes(graph.nodes)
       .on("tick", ticked);

   simulation.force("link")
       .links(graph.links);

    simulation.restart();

    node.on("click", onClick);

    function onClick(d) {
    console.log("on Click")
    tmp = this;
       test = d3.select(this).select("circle").attr("fill", function(d) {

        if (d.clicked) {
                d.clicked = false
                d3.selectAll("line[x2='"+d.x+"'][y2='"+d.y+"']").attr("class","")
                return color(1);

            } else {
                d.clicked = true;
                d3.selectAll("line[x2='"+d.x+"'][y2='"+d.y+"']").attr("class","red")

                return "#F00";
            }

        })
    }

   function ticked() {
     link
         .attr("x1", function(d) {  if (d.source.xxx) d.source.x = d.source.xxx; return d.source.x; })
         .attr("y1", function(d) { if (d.source.yyy) d.source.y = d.source.yyy;return d.source.y; })
         .attr("x2", function(d) { if (d.target.xxx) d.target.x = d.target.xxx; return d.target.x; })
         .attr("y2", function(d) { if (d.target.yyy) d.target.y = d.target.yyy; return d.target.y; });

     node
         .attr("cx", function(d) {
         if (d.xxx){
            d.x = d.xxx;
         }  return d.xxx; })
        .attr("cy", function(d) {if (d.yyy) {
            d.y = d.yyy;
        }  return d.yyy; })
         .attr("transform", function(d) {
           return "translate(" + d.x + "," + d.y + ")";
         }).
         attr("picked", function(d) {
            if (d.picked) {
            return 1;
            } else {
            return 0;
            }
         })
         .attr("fill", function(d) {
          if (d.picked) {
              d3.selectAll("line[x2='"+d.x+"'][y2='"+d.y+"']").attr("class","red")
              return "#F00";
          }
          else {
              return color(1)
          }
         })
   }
 });
}




function dragstarted(d) {
  if (!d3.event.active) simulation.alphaTarget(0.3).restart();
  d.fx = d.x;
  d.fy = d.y;
}

function dragged(d) {
  d.fx = d3.event.x;
  d.fy = d3.event.y;
}

function dragended(d) {
  if (!d3.event.active) simulation.alphaTarget(0);
  d.fx = null;
  d.fy = null;
}
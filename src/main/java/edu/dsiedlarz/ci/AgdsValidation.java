package edu.dsiedlarz.ci;

import edu.dsiedlarz.ci.agds.agds.AGDS;
import edu.dsiedlarz.ci.agds.agds.AgdsClass;
import edu.dsiedlarz.ci.agds.data.CsvDataLoader;
import edu.dsiedlarz.ci.agds.data.DataLoader;
import edu.dsiedlarz.ci.agds.data.GenericData;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class AgdsValidation {

    public static void main(String[] args) {

        DataLoader dataLoader = new CsvDataLoader();

        List<GenericData> trainData = dataLoader.loadData("data/iris/IrisTrain.csv");
        List<GenericData> testData = dataLoader.loadData("data/iris/IrisTest.csv");

        AGDS agds = AGDS.ofGenericData(trainData);

        AtomicReference<Double> guesses = new AtomicReference<>(0D);

        testData.forEach(test -> {
            AgdsClass agdsClass = agds.guessClassFromAttributes(test.getAttributes());

            if (agdsClass.getClassName().equals(test.getClassName())) {
                System.out.println("Guessed correctly");
                guesses.updateAndGet(v -> v + 1D);
            } else {
                System.out.println("Mismatch");
            }
        });

        System.out.println(String.format("Correctly guessed %.2f ", (guesses.get() / testData.size())));
    }
}

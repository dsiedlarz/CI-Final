package edu.dsiedlarz.ci.perceptron;


import edu.dsiedlarz.ci.agds.data.*;
import edu.dsiedlarz.ci.perceptron.model.Perceptron;
import edu.dsiedlarz.ci.perceptron.model.PerceptronGuess;

import java.util.List;
import java.util.stream.Collectors;

public class PerceptronApplication {

    public static void main(String[] args) {
        DataLoader dataLoader = new CsvDataLoader();

        List<GenericData> trainData = dataLoader.loadData("data/iris/IrisTrain.csv");
        List<GenericData> testData = dataLoader.loadData("data/iris/IrisTest.csv");

        trainData = IrisDataLoader.normalize(trainData);
        testData = IrisDataLoader.normalize(testData);

        Perceptron perceptron = new Perceptron(trainData);

        for (int i = 0; i < 500; i++) {
            perceptron.learn();
        }

        List<PerceptronGuess> perceptronGuesses = testData.stream().map(test -> {
            PerceptronGuess perceptronGuess = perceptron.guess(test);
            System.out.println(perceptronGuess);
            return perceptronGuess;
        }).collect(Collectors.toList());
    }
}

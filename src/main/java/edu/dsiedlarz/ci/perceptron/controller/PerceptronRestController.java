package edu.dsiedlarz.ci.perceptron.controller;

import edu.dsiedlarz.ci.agds.data.CsvDataLoader;
import edu.dsiedlarz.ci.agds.data.DataLoader;
import edu.dsiedlarz.ci.agds.data.GenericData;
import edu.dsiedlarz.ci.agds.data.IrisDataLoader;
import edu.dsiedlarz.ci.agds.validation.TestReport;
import edu.dsiedlarz.ci.perceptron.model.Perceptron;
import edu.dsiedlarz.ci.perceptron.model.PerceptronGuess;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.DecimalFormat;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class PerceptronRestController {


    @RequestMapping("/rest/mlp")
    @ResponseBody
    public TestReport mlp(@RequestParam(value = "sessions") Integer sessions) {
        DataLoader dataLoader = new CsvDataLoader();

        List<GenericData> trainData = dataLoader.loadData("data/iris/IrisTrain.csv");
        List<GenericData> testData = dataLoader.loadData("data/iris/IrisTest.csv");

        trainData = IrisDataLoader.normalize(trainData);
        testData = IrisDataLoader.normalize(testData);

        Perceptron perceptron = new Perceptron(trainData);

        for (int i = 0; i < sessions; i++) {
            perceptron.learn();
        }

        AtomicReference<Double> guesses = new AtomicReference<>(0D);
        AtomicReference<Double> sureness = new AtomicReference<>(0D);
        final String[] report = {""};
        DecimalFormat df = new DecimalFormat("0.00");

        testData.forEach(test -> {
            PerceptronGuess perceptronGuess = perceptron.guess(test);
            report[0] += test.getClassName() + "[" + Stream.of(test.getAttributes()).map(df::format).collect(Collectors.joining(",")) + "] ";

            if (perceptronGuess.guessed) {
                report[0] += "correctly guessed with probability : " + perceptronGuess.sureness;
                guesses.updateAndGet(v -> v + 1D);
                sureness.updateAndGet(v -> v + perceptronGuess.sureness);
            } else {
                report[0] += "Mismatch";
            }


            report[0] += "<br/>";

        });

        return new TestReport(guesses.get(), testData.size(), sureness.get() / testData.size(), report[0]);
    }
}

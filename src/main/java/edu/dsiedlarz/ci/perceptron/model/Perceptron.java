package edu.dsiedlarz.ci.perceptron.model;

import edu.dsiedlarz.ci.agds.data.GenericData;
import edu.dsiedlarz.ci.perceptron.model.neuron.AbstractNeuron;
import edu.dsiedlarz.ci.perceptron.model.neuron.Bias;
import edu.dsiedlarz.ci.perceptron.model.neuron.DataNeuron;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Perceptron {

    public static double LEARNING_RATE = 0.1;
    private List<GenericData> inputData = new ArrayList<>();
    private List<SubNetwork> subNetworks = new ArrayList<>();
    private List<AbstractNeuron> dataNeurons = new ArrayList<>();
    List<String> classes;

    public Perceptron(List<GenericData> inputData) {

        int inputDataNodes = inputData.get(0).getAttributes().length;
        classes = inputData.stream().map(GenericData::getClassName).distinct().collect(Collectors.toList());
        int classesCount = classes.size();


        this.inputData = inputData;

        dataNeurons = new ArrayList<>();
        for (int i = 0; i < inputDataNodes; i++) {
            dataNeurons.add(new DataNeuron());
        }
        dataNeurons.add(new Bias());

        for (int i = 0; i < 3; i++) {
            this.subNetworks.add(createSubNetwork(inputDataNodes, classesCount));
        }
    }

    public SubNetwork createSubNetwork(int inputDataNodes, int classesCount) {
        SubNetwork subNetwork = new SubNetwork(inputDataNodes, classesCount);


        for (int i = 0; i < this.dataNeurons.size(); i++) {
            for (int j = 0; j < subNetwork.inputNeurons.size(); j++) {
                new WeightedConnection(dataNeurons.get(i), subNetwork.inputNeurons.get(j));
            }
        }

        for (int i = 0; i < subNetworks.size(); i++) {
            for (int j = 0; j < subNetworks.get(i).outputNeurons.size(); j++) {
                for (int k = 0; k < subNetwork.inputNeurons.size(); k++) {
                    new WeightedConnection(subNetworks.get(i).outputNeurons.get(j), subNetwork.inputNeurons.get(j));
                }
            }
        }

        subNetwork.previousSubnetworks.addAll(this.subNetworks);
        return subNetwork;
    }


    public PerceptronGuess guess(GenericData genericData) {
        for (int i = 0; i < genericData.getAttributes().length; i++) {
            dataNeurons.get(i).outputValue = genericData.getAttributes()[i];
        }

        for (int i = 0; i < this.subNetworks.size(); i++) {
            subNetworks.get(i).propagationPhase();
        }

        SubNetwork subNetwork = subNetworks.get(subNetworks.size() -1);
        List<AbstractNeuron> outputs = subNetwork.getOutputNeurons();

        int highestIndex = 0;
        Double maxValue = Double.MIN_VALUE;
        for (int j = 0; j < subNetwork.outputNeurons.size(); j++) {
            Double outputValue = outputs.get(j).getOutputValue();
            if (outputValue > maxValue) {
                maxValue = outputValue;
                highestIndex = j;
            }
        }

        return new PerceptronGuess(classes.get(highestIndex), genericData.getClassName().equals(classes.get(highestIndex)), maxValue);
    }

    public List<Double> learn() {

        subNetworks.forEach(s -> {
            s.tries = 0;
            s.guessedValues = 0;
        });
        List<Double> efficiences = new ArrayList<>();
        inputData.forEach(input -> {

            for (int i = 0; i < input.getAttributes().length; i++) {
                dataNeurons.get(i).outputValue = input.getAttributes()[i];
            }

            for (int i = 0; i < this.subNetworks.size(); i++) {

                SubNetwork subNetwork = subNetworks.get(i);

                subNetwork.propagationPhase();
                List<AbstractNeuron> outputs = subNetwork.getOutputNeurons();

                subNetwork.tries++;
                int highestIndex = 0;
                Double maxValue = Double.MIN_VALUE;
                for (int j = 0; j < subNetwork.outputNeurons.size(); j++) {
                    Double outputValue = outputs.get(j).getOutputValue();
                    if (outputValue > maxValue) {
                        maxValue = outputValue;
                        highestIndex = j;
                    }
                }

                if (input.getClassName().equals(classes.get(highestIndex))) {
                    subNetwork.guessedValues++;
                }

                for (int j = 0; j < classes.size(); j++) {
                    if (input.getClassName().equals(classes.get(j))) {
                        outputs.get(j).setDelta(1D - outputs.get(j).getOutputValue());
                    } else {
                        outputs.get(j).setDelta(0D - outputs.get(j).getOutputValue());
                    }
                }

                subNetwork.backpropagationPhase();
            }
        });
        for (int i = 0; i < this.subNetworks.size(); i++) {
            SubNetwork subNetwork = subNetworks.get(i);
            efficiences.add(subNetwork.guessedValues / subNetwork.tries);
//            System.out.println("Efficiency " + i + " is " + (subNetwork.guessedValues / subNetwork.tries));
        }

        return efficiences;
    }
}

package edu.dsiedlarz.ci.perceptron.model;

public class PerceptronGuess {
    public final String className;
    public final boolean guessed;
    public final Double sureness;

    public PerceptronGuess(String className, boolean guessed, Double sureness) {
        this.className = className;
        this.guessed = guessed;
        this.sureness = sureness;
    }

    @Override
    public String toString() {
        return "PerceptronGuess{" +
                "className='" + className + '\'' +
                ", guessed=" + guessed +
                ", sureness=" + sureness +
                '}';
    }
}

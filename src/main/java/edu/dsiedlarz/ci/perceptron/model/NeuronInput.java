package edu.dsiedlarz.ci.perceptron.model;

public interface NeuronInput {
    Double getOutput();
}

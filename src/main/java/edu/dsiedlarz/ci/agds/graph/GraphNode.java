package edu.dsiedlarz.ci.agds.graph;

public class GraphNode {
    public final String id;
    public final int group;
    public final int xxx;
    public final int yyy;
    public boolean picked = false;


    public GraphNode(String id, int group, int xxx, int yyy) {
        this.id = id;
        this.group = group;
        this.xxx = xxx;
        this.yyy = yyy;
    }

    public void setPicked() {
        this.picked = true;
    }
}

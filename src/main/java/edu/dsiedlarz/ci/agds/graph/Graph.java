package edu.dsiedlarz.ci.agds.graph;

import edu.dsiedlarz.ci.agds.agds.AGDS;
import edu.dsiedlarz.ci.agds.agds.AgdsClass;
import edu.dsiedlarz.ci.agds.agds.AgdsElement;
import edu.dsiedlarz.ci.agds.avb.AvbPlusNode;
import edu.dsiedlarz.ci.agds.avb.AvbPlusTree;
import edu.dsiedlarz.ci.agds.avb.AvbPlusValue;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Graph {
    public List<GraphNode> nodes = new ArrayList<>();
    public List<GraphEdge> links = new ArrayList<>();

    private static DecimalFormat df = new DecimalFormat("0.00");

    public static Graph ofAGDS(AGDS agds) {
        Graph graph = new Graph();

        int position = 2;
        int subPosition = 1;


        boolean drawnClasses = false;

        for (int i = 0; i < agds.attributes.size(); i++) {

            if (!drawnClasses && i >= (agds.attributes.size() / 2)) {
                drawnClasses = true;

                List<AgdsClass> classes = agds.classes.getAllValues();
                for (int j = 0; j < classes.size(); j++) {
                    graph.nodes.add(new GraphNode(classes.get(j).getClassName() + "[" + df.format(classes.get(j).getX()) + "]", 1, (subPosition++) * 100, 300));
                    graph.links.add(new GraphEdge("class", classes.get(j).getClassName() + "[" + df.format(classes.get(j).getX()) + "]", 1));
                }
                subPosition++;
            }
            String id = "attr-" + i;

            List<AvbPlusValue<Double>> values = agds.attributes.get(i).getAllValues();
            for (int j = 0; j < values.size(); j++) {
                String subId = id + "-" + df.format(values.get(j).getValue()) + "[" + df.format(values.get(j).getX()) + "]";
                graph.nodes.add(new GraphNode(subId, 1, (subPosition++) * 100, 300));
                graph.links.add(new GraphEdge(id, subId, 1));
            }
            subPosition++;

        }

        int space = subPosition * 100 / (1 + agds.attributes.size());
        space = Double.valueOf(space * 0.7).intValue();

        drawnClasses = false;
        for (int i = 0; i < agds.attributes.size(); i++) {
            String id = "attr-" + i;

            if (!drawnClasses && i >= (agds.attributes.size() / 2)) {
                drawnClasses = true;
                graph.nodes.add(new GraphNode("class", 1, (position++) * space, 200));
                graph.links.add(new GraphEdge("root", "class", 1));
            }

            graph.nodes.add(new GraphNode(id, 1, (position++) * space, 200));
            graph.links.add(new GraphEdge("root", id, 1));

        }

        space = Double.valueOf(space / 0.7).intValue();

        graph.nodes.add(new GraphNode("root", 1, (position * space) / 2 - 100, 100));


        position = 0;
        space = subPosition * 100 / agds.elements.size();

        for (int i = 0; i < agds.elements.size(); i++) {
            AgdsElement agdsElement = agds.elements.get(i);
            String nodeId = agdsElement.getName() + "[" + df.format(agdsElement.getX()) + "]";

            graph.nodes.add(new GraphNode(nodeId, 1, 100 + (position++) * space, 600));

            graph.links.add(new GraphEdge(agdsElement.getElementClass().getClassName() + "[" + df.format(agdsElement.getElementClass().getX()) + "]", nodeId, 1));

            for (int j = 0; j < agdsElement.getAttributes().size(); j++) {
                String id = "attr-" + j + "-";

                String attrId = id + df.format(agdsElement.getAttributes().get(j).getValue()) + "[" + df.format(agdsElement.getAttributes().get(j).getX()) + "]";


                graph.links.add(new GraphEdge(attrId, nodeId, 1));
            }
        }

        return graph;
    }

    public static Graph ofAvbPlusTree(AvbPlusTree<Double> avbPlusTree) {
        Graph graph = new Graph();
        int counter = 0;

        String node = avbPlusTree.getRoot().getGraphString();
        graph.nodes.add(new GraphNode(node, 1, 400, 400));

        avbPlusTree.getRoot().getChildren().forEach(el -> addAvbNodes(graph, el));

        return graph;
    }

    public static void addAvbNodes(Graph graph, AvbPlusNode<Double> node) {

        graph.nodes.add(new GraphNode(node.getGraphString(), 1, 0, 0));
        graph.links.add(new GraphEdge(node.getParent().getGraphString(), node.getGraphString(), 1));

        node.getChildren().forEach(el -> addAvbNodes(graph, el));
    }
}

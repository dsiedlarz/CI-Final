package edu.dsiedlarz.ci.agds.graph;

public class GraphEdge {

    public final String source;
    public final String target;
    public final int value;

    public GraphEdge(String source, String target, int value) {
        this.source = source;
        this.target = target;
        this.value = value;
    }
}

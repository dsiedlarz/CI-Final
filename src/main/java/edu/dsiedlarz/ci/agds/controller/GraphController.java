package edu.dsiedlarz.ci.agds.controller;

import edu.dsiedlarz.ci.agds.agds.AGDS;
import edu.dsiedlarz.ci.agds.agds.AgdsClass;
import edu.dsiedlarz.ci.agds.agds.AgdsElement;
import edu.dsiedlarz.ci.agds.data.*;
import edu.dsiedlarz.ci.agds.graph.Graph;
import edu.dsiedlarz.ci.agds.validation.TestReport;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class GraphController {

    @RequestMapping("/rest/sample-graph")
    @ResponseBody
    public Graph graph() {
        IrisDataLoader irisDataLoader = new IrisDataLoader("Iris3.csv");
        List<IrisData> irisDataList = irisDataLoader.loadData().subList(0, 20);

        AGDS agds = new AGDS();

        for (int i = 0; i < irisDataList.size(); i++) {
            IrisData irisData = irisDataList.get(i);
            agds.put("R" + i, irisData.name, irisData.values.toArray(new Double[irisData.values.size()]));
        }

        return Graph.ofAGDS(agds);
    }

    @RequestMapping("/rest/most-similar-node")
    @ResponseBody
    public Graph mostSimilarNode(
            @RequestParam("attr1") Double attr1,
            @RequestParam("attr2") Double attr2,
            @RequestParam("attr3") Double attr3,
            @RequestParam("attr4") Double attr4

    ) {
        IrisDataLoader irisDataLoader = new IrisDataLoader("Iris3.csv");
        List<IrisData> irisDataList = irisDataLoader.loadData().subList(0, 20);

        AGDS agds = new AGDS();

        for (int i = 0; i < irisDataList.size(); i++) {
            IrisData irisData = irisDataList.get(i);
            agds.put("R" + i, irisData.name, irisData.values.toArray(new Double[irisData.values.size()]));
        }


        AgdsElement agdsElement = agds.findMostSimilarElement(attr1, attr2, attr3, attr4);

        Graph graph = Graph.ofAGDS(agds);
        graph.nodes.parallelStream().forEach(node -> {
            if (node.id.contains(agdsElement.getName() + "[")) {
                node.setPicked();
            }
        });

        return graph;
    }

    @RequestMapping("/rest/guess-class")
    @ResponseBody
    public AgdsClass guessClass(
            @RequestParam("attr1") Double attr1,
            @RequestParam("attr2") Double attr2,
            @RequestParam("attr3") Double attr3,
            @RequestParam("attr4") Double attr4

    ) {
        IrisDataLoader irisDataLoader = new IrisDataLoader("Iris3.csv");
        List<IrisData> irisDataList = irisDataLoader.loadData();

        AGDS agds = new AGDS();

        for (int i = 0; i < irisDataList.size(); i++) {
            IrisData irisData = irisDataList.get(i);
            agds.put("R" + i, irisData.name, irisData.values.toArray(new Double[irisData.values.size()]));
        }


        AgdsClass agdsElement = agds.guessClassFromAttributes(attr1, attr2, attr3, attr4);

        return agdsElement;
    }

    @RequestMapping("/rest/avb-tree")
    @ResponseBody
    public Graph graph2() {
        IrisDataLoader irisDataLoader = new IrisDataLoader("Iris3.csv");
        List<IrisData> irisDataList = irisDataLoader.loadData();

        AGDS agds = new AGDS();

        for (int i = 0; i < irisDataList.size(); i++) {
            IrisData irisData = irisDataList.get(i);

            agds.put("R" + i, irisData.name, irisData.values.toArray(new Double[irisData.values.size()]));
        }

        System.out.println(agds.attributes.get(0).getAllValues().stream().map(el -> el.getValue().toString()).collect(Collectors.joining(" ")));

        return Graph.ofAvbPlusTree(agds.attributes.get(0));
    }


    @RequestMapping("/rest/agds-test")
    @ResponseBody
    public TestReport test() {
        DataLoader dataLoader = new CsvDataLoader();

        List<GenericData> trainData = dataLoader.loadData("data/iris/IrisTrain.csv");
        List<GenericData> testData = dataLoader.loadData("data/iris/IrisTest.csv");

        AGDS agds = AGDS.ofGenericData(trainData);

        AtomicReference<Double> guesses = new AtomicReference<>(0D);
        AtomicReference<Double> sureness = new AtomicReference<>(0D);
        final String[] report = {""};

        testData.forEach(test -> {
            AgdsClass agdsClass = agds.guessClassFromAttributes(test.getAttributes());

            report[0] += test.getClassName() +"[" + Stream.of(test.getAttributes()).map(Object::toString).collect(Collectors.joining(",")) + "] ";

            if (agdsClass.getClassName().equals(test.getClassName())) {
                report[0] += "correctly guessed with probability : " + agdsClass.getX();
                guesses.updateAndGet(v -> v + 1D);
                sureness.updateAndGet(v -> v + agdsClass.getX());
            } else {
                report[0] +="Mismatch";
            }

            report[0] += "<br/>";
        });

        return new TestReport(guesses.get(), testData.size(), sureness.get() / testData.size(), report[0]);
    }

}

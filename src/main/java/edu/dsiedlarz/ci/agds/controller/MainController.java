package edu.dsiedlarz.ci.agds.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {


    @GetMapping("/")
    public String index(Model model) {
        return "index";
    }

    @GetMapping("/tree")
    public String tree(Model model) {
        return "tree";
    }

    @GetMapping("/mlp")
    public String mlp(Model model) {
        return "mlp";
    }

}


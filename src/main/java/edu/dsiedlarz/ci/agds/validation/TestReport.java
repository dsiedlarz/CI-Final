package edu.dsiedlarz.ci.agds.validation;

public class TestReport {
    public final Double guessedValues;
    public final int total;
    public final Double meanSureness;
    public final String report;

    public TestReport(Double guessedValues, int total, Double meanSureness, String report) {
        this.guessedValues = guessedValues;
        this.total = total;
        this.meanSureness = meanSureness;
        this.report = report;
    }
}

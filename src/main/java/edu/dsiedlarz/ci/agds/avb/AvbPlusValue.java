package edu.dsiedlarz.ci.agds.avb;

import edu.dsiedlarz.ci.agds.agds.SimiliarityX;

public class AvbPlusValue<T extends Number & Comparable<T>> extends SimiliarityX {
    private final T value;
    int counter;
    public final AvbPlusTree<T> tree;

    AvbPlusValue<T> biggerNeighbour;
    AvbPlusValue<T> smallerNeighbour;

    public AvbPlusValue(T value, AvbPlusTree<T> tree) {
        this.value = value;
        this.tree = tree;
        counter = 1;
    }

    public AvbPlusValue<T> increment() {
        counter++;
        return this;
    }

    public int getCounter() {
        return counter;
    }

    public void setBiggerNeighbour(AvbPlusValue<T> biggerNeighbour) {

        if (this.biggerNeighbour != null) {
            this.biggerNeighbour.smallerNeighbour = biggerNeighbour;
            biggerNeighbour.biggerNeighbour = this.biggerNeighbour;
        } else if (biggerNeighbour.smallerNeighbour != null) {
            biggerNeighbour.smallerNeighbour.biggerNeighbour = this;
            this.smallerNeighbour = biggerNeighbour.smallerNeighbour;

        }

        this.biggerNeighbour = biggerNeighbour;

        biggerNeighbour.smallerNeighbour = this;
    }

    private void setSmallerNeighbour(AvbPlusValue<T> smallerNeighbour) {
        if (this.smallerNeighbour != null) {
            this.smallerNeighbour.biggerNeighbour = smallerNeighbour;
            smallerNeighbour.smallerNeighbour = this.smallerNeighbour;
        }
        this.biggerNeighbour = smallerNeighbour;

        biggerNeighbour.smallerNeighbour = this;
    }

    public T getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "AvbPlusValue{" +
                "value=" + value +
                ", counter=" + counter +
                '}';
    }

    public AvbPlusValue<T> getBiggerNeighbour() {
        return biggerNeighbour;
    }

    public AvbPlusValue<T> getSmallerNeighbour() {
        return smallerNeighbour;
    }

}

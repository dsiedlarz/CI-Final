package edu.dsiedlarz.ci.agds.avb;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AvbPlusNode<T extends Number & Comparable<T>> {
    List<AvbPlusValue<T>> values = new ArrayList<>();
    List<AvbPlusNode<T>> children = new ArrayList<>();
    AvbPlusNode<T> parent;
    AvbPlusTree<T> tree;

    public AvbPlusNode(AvbPlusTree<T> tree) {
        this.tree = tree;
        this.tree.root = this;
        this.parent = null;
    }

    public AvbPlusNode(AvbPlusTree<T> tree, AvbPlusNode<T> parent) {
        this.tree = tree;
        this.parent = parent;
    }

    public AvbPlusValue<T> insert(T value) {
        if (values.isEmpty()) {
            AvbPlusValue<T> avbPlusValue = new AvbPlusValue<>(value, tree);
            values.add(avbPlusValue);
            return avbPlusValue;
        }

        if (values.size() == 1) {

            if (values.get(0).getValue().equals(value)) {
                return values.get(0).increment();
            }

            if (!children.isEmpty()) {
                //left node
                if (this.getSmallestValue().compareTo(value) > 0) {
                    return this.children.get(0).insert(value);
                }
                // right node
                if (this.getBiggestValue().compareTo(value) < 0) {
                    return this.children.get(1).insert(value);
                }
                //middle node
                return this.children.get(1).insert(value);
            }

            AvbPlusValue<T> avbPlusValue = new AvbPlusValue<>(value, tree);


            this.tryToSetNeighbourHood(avbPlusValue);

            this.insert(avbPlusValue);

            return avbPlusValue;
        }

        if (values.size() == 2) {

            if (values.get(0).getValue().equals(value)) {
                return values.get(0).increment();
            }

            if (values.get(1).getValue().equals(value)) {
                return values.get(1).increment();
            }

            if (!children.isEmpty()) {
                //left node
                if (this.getSmallestValue().compareTo(value) > 0) {
                    return this.children.get(0).insert(value);
                }
                // right node
                if (this.getBiggestValue().compareTo(value) < 0) {
                    return this.children.get(2).insert(value);
                }
                //middle node
                return this.children.get(1).insert(value);
            }

            AvbPlusValue<T> avbPlusValue = new AvbPlusValue<>(value, tree);

            this.tryToSetNeighbourHood(avbPlusValue);

            this.insert(avbPlusValue);

            insertToParent(this);

            return avbPlusValue;
        }

        throw new RuntimeException("Something went wrong");
    }

    //TODO refactor this for sure
    private void tryToSetNeighbourHood(AvbPlusValue<T> newValue) {
        ArrayList<AvbPlusValue<T>> elements = new ArrayList<>();
        elements.addAll(values);
        if (parent != null) {
            elements.addAll(parent.values);
        }

        AvbPlusValue<T> t = elements
                .stream()
                .filter(e -> e.getValue().compareTo(newValue.getValue()) > 0)
                .min(Comparator.comparing(AvbPlusValue::getValue)).orElseGet(() ->
                        elements
                                .stream()
                                .filter(e -> e.getValue().compareTo(newValue.getValue()) < 0)
                                .max(Comparator.comparing(AvbPlusValue::getValue)).get()
                );

        if (t.getValue().compareTo(newValue.getValue()) > 0) {
            newValue.setBiggerNeighbour(t);
        } else {
            t.setBiggerNeighbour(newValue);
        }
    }

    private void insertToParent(AvbPlusNode<T> node) {

        if (node.values.size() != 3) {
            throw new RuntimeException("insertToParent() The value must be 3");
        }

        if (parent == null) {
            parent = new AvbPlusNode<T>(tree);
        }


        AvbPlusNode<T> newRightNode = new AvbPlusNode<T>(tree, parent);

        newRightNode.insert(node.values.get(2));
        node.values.remove(2);

        if (children.size() == 4) {
            children.get(3).parent = newRightNode;
            newRightNode.insertChildren(children.get(3));
            children.remove(3);
            children.get(2).parent = newRightNode;
            newRightNode.insertChildren(children.get(2));
            children.remove(2);
        }

        parent.insert(node.values.get(1));
        node.values.remove(1);

        if (!parent.children.contains(node)) {
            parent.insertChildren(node);
        }

        parent.insertChildren(newRightNode);

        if (parent.values.size() == 3) {
            parent.insertToParent(parent);
        }
    }

    void insertChildren(AvbPlusNode<T> node) {
        if (children.isEmpty()) {
            children.add(node);
            return;
        }

        if (children.size() == 1) {
            if (children.get(0).getSmallestValue().compareTo(node.getBiggestValue()) > 0) {
                children.add(0, node);
                return;
            }
            children.add(1, node);
            return;
        }

        if (children.size() == 2) {
            if (children.get(0).getSmallestValue().compareTo(node.getBiggestValue()) > 0) {
                children.add(0, node);
                return;
            }
            if (children.get(1).getBiggestValue().compareTo(node.getSmallestValue()) < 0) {
                children.add(2, node);
                return;
            }
            children.add(1, node);
            return;
        }

        if (children.size() == 3) {
            if (children.get(0).getSmallestValue().compareTo(node.getBiggestValue()) > 0) {
                children.add(0, node);
                return;
            }

            if (children.get(2).getBiggestValue().compareTo(node.getSmallestValue()) < 0) {
                children.add(3, node);
                return;
            }

            if (children.get(0).getBiggestValue().compareTo(node.getSmallestValue()) < 0
                    &&
                    children.get(1).getSmallestValue().compareTo(node.getBiggestValue()) > 0
                    ) {
                children.add(1, node);
                return;
            }


            children.add(2, node);

        }
    }

    public void insert(AvbPlusValue<T> value) {
        if (values.isEmpty()) {
            values.add(0, value);
            return;
        }
        if (values.size() == 1) {
            if (values.get(0).getValue().compareTo(value.getValue()) > 0) {
                values.add(0, value);
            } else {
                values.add(1, value);
            }
            return;
        }

        if (values.size() == 2) {
            if (values.get(0).getValue().compareTo(value.getValue()) > 0) {
                values.add(0, value);
            } else if (values.get(1).getValue().compareTo(value.getValue()) < 0) {
                values.add(2, value);
            } else {
                values.add(1, value);
            }
        }
    }

    public T getSmallestValue() {
        return values.stream().min(Comparator.comparing(AvbPlusValue::getValue)).get().getValue();
    }

    public T getBiggestValue() {
        return values.stream().max(Comparator.comparing(AvbPlusValue::getValue)).get().getValue();
    }

    @Override
    public String toString() {
        return "AvbPlusNode{" +
                "values=" + values +
                '}';
    }

    public AvbPlusValue<T> findClosestValue(T value) {

        for (int i = 0; i < values.size(); i++) {
            if (values.get(i).getValue().equals(value)) {
                return values.get(i);
            }
        }

        if (children.isEmpty()) {
            if (values.size() == 1) {
                return checkClosestInNeighbourhood(values.get(0), value);
            }

            if (Math.abs(value.doubleValue() - values.get(0).getValue().doubleValue()) < Math.abs(value.doubleValue() - values.get(1).getValue().doubleValue())) {
                return checkClosestInNeighbourhood(values.get(0), value);
            } else {
                return checkClosestInNeighbourhood(values.get(1), value);
            }
        } else {
            if (children.size() == 2) {
                if (values.get(0).getValue().compareTo(value) > 0) {
                    return children.get(0).findClosestValue(value);
                } else {
                    return children.get(1).findClosestValue(value);
                }
            } else {

                if (values.get(0).getValue().compareTo(value) > 0) {
                    return children.get(0).findClosestValue(value);
                } else if (values.get(1).getValue().compareTo(value) < 0) {
                    return children.get(2).findClosestValue(value);
                } else {
                    return children.get(1).findClosestValue(value);
                }
            }
        }
    }

    private AvbPlusValue<T> checkClosestInNeighbourhood(AvbPlusValue<T> avbPlusValue, T value) {

        if (avbPlusValue.getSmallerNeighbour() != null) {
            if (Math.abs(value.doubleValue() - avbPlusValue.getSmallerNeighbour().getValue().doubleValue()) <= Math.abs(value.doubleValue() - avbPlusValue.getValue().doubleValue())) {
                return avbPlusValue.getSmallerNeighbour();
            }
        }

        if (avbPlusValue.getBiggerNeighbour() != null) {
            if (Math.abs(value.doubleValue() - avbPlusValue.getBiggerNeighbour().getValue().doubleValue()) < Math.abs(value.doubleValue() - avbPlusValue.getValue().doubleValue())) {
                return avbPlusValue.getBiggerNeighbour();
            }
        }

        return avbPlusValue;
    }

    public List<AvbPlusValue<T>> getValues() {
        return values;
    }

    public List<AvbPlusNode<T>> getChildren() {
        return children;
    }

    public AvbPlusNode<T> getParent() {
        return parent;
    }

    public String getGraphString() {
        return "|" + values.stream().map(el -> el.getValue().toString()).collect(Collectors.joining(" ")) + "|";
    }
}

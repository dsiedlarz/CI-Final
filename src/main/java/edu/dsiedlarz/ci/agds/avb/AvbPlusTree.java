package edu.dsiedlarz.ci.agds.avb;

import java.util.ArrayList;
import java.util.List;

public class AvbPlusTree<T extends Number & Comparable<T>> {

    AvbPlusNode<T> root = new AvbPlusNode<>(this);
    AvbPlusValue<T> min = null;
    AvbPlusValue<T> max = null;

    public AvbPlusValue<T> insert(T value) {
        AvbPlusValue<T> v = root.insert(value);
        updateMinAndMaxValue(v);
        return v;
    }

    private void updateMinAndMaxValue(AvbPlusValue<T> value) {
        if (min == null) {
            min = value;
        }

        if (max == null) {
            max = value;
        }

        if (value.getValue().compareTo(min.getValue()) < 0 ) {
            min = value;
        }

        if (value.getValue().compareTo(max.getValue()) > 0 ) {
            max = value;
        }
    }

    public AvbPlusValue<T> getMin() {
        return min;
    }

    public AvbPlusValue<T> getMax() {
        return max;
    }

    public List<AvbPlusValue<T>> getAllValues() {
        List<AvbPlusValue<T>> values = new ArrayList<>();
        AvbPlusValue<T> v = min;

        while (v != null) {
            values.add(v);
            v = v.biggerNeighbour;
        }

        return values;
    }

    public AvbPlusValue<T> findClosestValue(T value) {

        if (value.compareTo(min.getValue()) < 0) {
            return min;
        }

        if (value.compareTo(max.getValue()) > 0) {
            return max;
        }


        return root.findClosestValue(value);
    }

    public AvbPlusNode<T> getRoot() {
        return root;
    }
}

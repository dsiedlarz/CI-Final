package edu.dsiedlarz.ci.agds.agds;


import edu.dsiedlarz.ci.agds.avb.AvbPlusValue;

import java.util.ArrayList;
import java.util.List;

public class SimiliarityX {
    Double x;
    boolean hasSomethingToCompute = true;

    SimiliarityX leftObject;
    SimiliarityX rightObject;
    SimiliarityX classObject;

    List<SimiliarityX> downObjects = new ArrayList<>();
    List<SimiliarityX> upAttributesObjects = new ArrayList<>();


    public SimiliarityX() {
        x = 0D;
    }

    public void setLeftObject(SimiliarityX leftObject) {
        this.leftObject = leftObject;
    }

    public void setRightObject(SimiliarityX rightObject) {
        this.rightObject = rightObject;
    }

    public void addDownObject(SimiliarityX downObject) {
        this.downObjects.add(downObject);
    }

    public void addUpAttributesObjects(SimiliarityX upObject) {
        this.upAttributesObjects.add(upObject);
    }

    public void resetSimilarity() {
        this.x = 0D;
        this.hasSomethingToCompute = true;
    }

    public void setX(Double x) {
        if (x > 1D) {
            throw new RuntimeException("Probability cannot be more than 1");
        }

        if (x < 0) {
            throw new RuntimeException("Probability cannot be less than 0");
        }

        this.x = x;
    }

    public Double getX() {
        return x;
    }

    public void setClassObject(SimiliarityX classObject) {
        this.classObject = classObject;
    }

    public void computeClassProbability() {
        setX(downObjects.parallelStream().mapToDouble(SimiliarityX::getX).sum() / downObjects.size());
    }


    public void propagateWeights() {
        upAttributesObjects.stream().filter(up -> up.x == 0).forEach(up -> up.setX(x));
        if (classObject.x == 0) {
            classObject.setX(x);
        }

        upAttributesObjects.forEach(up -> {
            up.computeLeftNeighbourWeight();
            up.computeRightNeighbourWeight();
        });
    }

    public void computeLeftNeighbourWeight() {
        if (leftObject != null && leftObject.x == 0D) {
            leftObject.setX(x * AGDS.computeNegihbourConnectionWeight((AvbPlusValue) this, (AvbPlusValue) leftObject));
            leftObject.computeLeftNeighbourWeight();
        }
    }

    public void computeRightNeighbourWeight() {
        if (rightObject != null && rightObject.x == 0D) {
            rightObject.setX(x * AGDS.computeNegihbourConnectionWeight((AvbPlusValue) this, (AvbPlusValue) rightObject));
            rightObject.computeRightNeighbourWeight();
        }
    }

    public void propagateFromUp() {
        Integer factors = 1 + upAttributesObjects.size();
        Double weight = 0D;
        weight += classObject.getX() / factors;

        for (int i = 0; i < upAttributesObjects.size(); i++) {
            weight += upAttributesObjects.get(i).getX() / factors;
        }

        setX(weight);
    }

}

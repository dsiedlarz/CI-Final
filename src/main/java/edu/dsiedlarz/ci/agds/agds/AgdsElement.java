package edu.dsiedlarz.ci.agds.agds;

import edu.dsiedlarz.ci.agds.avb.AvbPlusValue;

import java.util.List;

public class AgdsElement extends SimiliarityX {

    private final String name;
    private final AgdsClass elementClass;
    private final List<AvbPlusValue<Double>> attributes;

    public AgdsElement(String name, AgdsClass elementClass, List<AvbPlusValue<Double>> attributes) {
        this.name = name;
        this.elementClass = elementClass;
        this.attributes = attributes;
    }

    public String getName() {
        return name;
    }

    public AgdsClass getElementClass() {
        return elementClass;
    }

    public List<AvbPlusValue<Double>> getAttributes() {
        return attributes;
    }

    @Override
    public String toString() {
        return "["+name + " " + elementClass.getClassName() + " " + x +"]";
    }
}

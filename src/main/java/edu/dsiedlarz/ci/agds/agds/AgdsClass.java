package edu.dsiedlarz.ci.agds.agds;

import java.util.Objects;

public class AgdsClass extends SimiliarityX {
    private final String className;

    public AgdsClass(String className) {
        this.className = className;
    }

    public String getClassName() {
        return className;
    }

    @Override
    public String toString() {
        return "AgdsClass{" + className + "[" + getX() + "]}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AgdsClass agdsClass = (AgdsClass) o;
        return Objects.equals(className, agdsClass.className);
    }

    @Override
    public int hashCode() {

        return Objects.hash(className);
    }
}

package edu.dsiedlarz.ci.agds.agds;

import edu.dsiedlarz.ci.agds.avb.AvbPlusTree;
import edu.dsiedlarz.ci.agds.avb.AvbPlusValue;
import edu.dsiedlarz.ci.agds.data.GenericData;
import edu.dsiedlarz.ci.agds.data.IrisData;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AGDS {

    public ClassSet classes = new ClassSet();
    public List<AvbPlusTree<Double>> attributes = new ArrayList<>();
    public List<AgdsElement> elements = new ArrayList<>();

    public static AGDS ofGenericData(List<GenericData> genericDataList) {
        AGDS agds = new AGDS();

        for (int i = 0; i < genericDataList.size(); i++) {
            GenericData genericData = genericDataList.get(i);
            AgdsElement element = agds.put("R" + i, genericData.getClassName(), genericData.getAttributes());
        }

        return agds;
    }

    public AgdsElement put(String elementName, String elementClass, Double... attributesValues) {

        AgdsClass agdsClass = classes.getOrCreate(elementClass);
        List<AvbPlusValue<Double>> newAttributes = new ArrayList<>();


        for (int i = 0; i < attributesValues.length; i++) {
            if (i >= attributes.size()) {
                attributes.add(new AvbPlusTree<>());
            }

            AvbPlusValue<Double> avbPlusValue = attributes.get(i).insert(attributesValues[i]);
            newAttributes.add(avbPlusValue);
        }

        AgdsElement agdsElement = new AgdsElement(elementName, agdsClass, newAttributes);
        elements.add(agdsElement);


        newAttributes.forEach(attribute -> {
            attribute.addDownObject(agdsElement);
            agdsElement.addUpAttributesObjects(attribute);

            if (attribute.getSmallerNeighbour() != null) {
                attribute.setLeftObject(attribute.getSmallerNeighbour());
                attribute.getSmallerNeighbour().setRightObject(attribute);
            }

            if (attribute.getBiggerNeighbour() != null) {
                attribute.setRightObject(attribute.getBiggerNeighbour());
                attribute.getBiggerNeighbour().setLeftObject(attribute);
            }

        });

        agdsClass.addDownObject(agdsElement);
        agdsElement.setClassObject(agdsClass);

        return agdsElement;
    }


    public Double computeLeftNeigbourConnectionWeight(AvbPlusValue<Double> value) {
        AvbPlusValue<Double> neighbour = value.getSmallerNeighbour();

        if (neighbour == null) {
            return null;
        }

        return computeNegihbourConnectionWeight(value, neighbour);
    }

    public Double computeRightNeigbourConnectionWeight(AvbPlusValue<Double> value) {
        AvbPlusValue<Double> neighbour = value.getBiggerNeighbour();

        if (neighbour == null) {
            return null;
        }

        return computeNegihbourConnectionWeight(value, neighbour);
    }

    public static Double computeNegihbourConnectionWeight(AvbPlusValue<Double> value, AvbPlusValue<Double> neighbour) {
        Double min = value.tree.getMin().getValue();
        Double max = value.tree.getMax().getValue();

        return 1 - (Math.abs(value.getValue() - neighbour.getValue()) / (max - min));
    }

    public Double getDownConnectionWeights(AvbPlusValue<Double> value) {
        return 1D / value.getCounter();
    }

    public void resetSimilarities() {
        elements.parallelStream().forEach(element -> {
            element.getElementClass().resetSimilarity();

            element.getAttributes().forEach(SimiliarityX::resetSimilarity);
        });
    }

    public void computeSimilarities(AgdsElement agdsElement) {
        resetSimilarities();
        agdsElement.setX(1D);
        agdsElement.propagateWeights();
        elements.parallelStream().forEach(SimiliarityX::propagateFromUp);
    }

    public AgdsClass guessClassFromAttributes(Double... attrs) {
        resetSimilarities();
        propagateSimilaritiesFromPassedValuesToElements(attrs);

        classes.getAllValues().parallelStream().forEach(SimiliarityX::computeClassProbability);
        return classes.getAllValues().stream().max(Comparator.comparingDouble(SimiliarityX::getX)).get();
    }

    public AgdsElement findMostSimilarElement(Double... attrs) {
        resetSimilarities();
        propagateSimilaritiesFromPassedValuesToElements(attrs);

        return elements.parallelStream().max(Comparator.comparingDouble(SimiliarityX::getX)).get();
    }

    private void propagateSimilaritiesFromPassedValuesToElements(Double... attrs) {
        for (int i = 0; i < attrs.length; i++) {
            AvbPlusValue<Double> avbPlusValue = attributes.get(i).findClosestValue(attrs[i]);

            if (avbPlusValue.getValue().equals(attrs[i])) {
                avbPlusValue.setX(1D);
                avbPlusValue.computeLeftNeighbourWeight();
                avbPlusValue.computeRightNeighbourWeight();
            }

            if (avbPlusValue.getValue().compareTo(attrs[i]) > 0) {
                if (avbPlusValue.getSmallerNeighbour() == null) {
                    avbPlusValue.setX(1D);
                    avbPlusValue.computeRightNeighbourWeight();
                } else {
                    Double leftWeight = 1 - (Math.abs(attrs[i] - avbPlusValue.getSmallerNeighbour().getValue()) / (Math.abs(avbPlusValue.getValue() - avbPlusValue.getSmallerNeighbour().getValue())));
                    avbPlusValue.getSmallerNeighbour().setX(leftWeight);
                    avbPlusValue.getSmallerNeighbour().computeLeftNeighbourWeight();

                    Double rightWeight = 1 - (Math.abs(attrs[i] - avbPlusValue.getValue()) / (Math.abs(avbPlusValue.getValue() - avbPlusValue.getSmallerNeighbour().getValue())));
                    avbPlusValue.setX(rightWeight);
                    avbPlusValue.computeRightNeighbourWeight();
                }
            }


            if (avbPlusValue.getValue().compareTo(attrs[i]) < 0) {
                if (avbPlusValue.getBiggerNeighbour() == null) {
                    avbPlusValue.setX(1D);
                    avbPlusValue.computeLeftNeighbourWeight();
                } else {
                    Double leftWeight = 1 - (Math.abs(attrs[i] - avbPlusValue.getValue()) / (Math.abs(avbPlusValue.getValue() - avbPlusValue.getBiggerNeighbour().getValue())));
                    avbPlusValue.setX(leftWeight);
                    avbPlusValue.computeLeftNeighbourWeight();

                    Double rightWeight = 1 - (Math.abs(attrs[i] - avbPlusValue.getBiggerNeighbour().getValue()) / (Math.abs(avbPlusValue.getValue() - avbPlusValue.getBiggerNeighbour().getValue())));
                    avbPlusValue.getBiggerNeighbour().setX(rightWeight);
                    avbPlusValue.getBiggerNeighbour().computeRightNeighbourWeight();
                }
            }
        }

        elements.parallelStream().forEach(SimiliarityX::propagateFromUp);
    }


    @Override
    public String toString() {

        DecimalFormat df = new DecimalFormat("0.00");

        String classesString = classes.getAllValues().stream().map(el -> "[" + el.getClassName() + " " + df.format(el.getX()) + "]").collect(Collectors.joining(" "));


        List<String> attributesStringList = attributes.stream().map(attributesList -> {
            return attributesList.getAllValues().stream().map(el -> "[" + el.getValue() + " " + df.format(el.getX()) + "]").collect(Collectors.joining(" "));
        }).collect(Collectors.toList());


        return String.format("%1$" + (classesString.length() / 2 - 3) + "s", "")
                + "classes: " +
                String.format("%1$" + (classesString.length() / 2 - 3) + "s", "") +


                attributesStringList.stream().map(el -> String.format("%1$" + ((el.length() / 2)) + "s", "") +
                        "attr:" +
                        String.format("%1$" + ((el.length() / 2)) + "s", "")).collect(Collectors.joining("")) +

                "\n" + classesString + "     " +

                attributesStringList.stream().collect(Collectors.joining("     ")) +

                "\n\n\n " + elements.parallelStream().map(AgdsElement::toString).collect(Collectors.joining(" "));
    }
}

package edu.dsiedlarz.ci.agds.agds;

import java.util.ArrayList;
import java.util.List;

public class ClassSet {
    List<AgdsClass> classes = new ArrayList<>();

    public AgdsClass getOrCreate(String className) {

        for (int i = 0; i < classes.size(); i++) {
            if (classes.get(i).getClassName().equals(className)) {
                return classes.get(i);
            }
        }

        AgdsClass agdsClass = new AgdsClass(className);
        classes.add(agdsClass);
        return agdsClass;
    }


    public List<AgdsClass> getAllValues() {
        return classes;
    }
}

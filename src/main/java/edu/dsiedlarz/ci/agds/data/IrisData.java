package edu.dsiedlarz.ci.agds.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IrisData {

    public static List<String> classes = Arrays.asList("Iris-setosa", "Iris-versicolor", "Iris-virginica");

    public List<Double> values = new ArrayList<>();

    public final String name;

    public IrisData(Double var1, Double var2, Double var3, Double var4, String name) {
        values.add(var1);
        values.add(var2);
        values.add(var3);
        values.add(var4);
        this.name = name;
    }

    public static IrisData ofLine(String line) {
        String[] values = line.split(",");
        return new IrisData(
                Double.valueOf(values[0]),
                Double.valueOf(values[1]),
                Double.valueOf(values[2]),
                Double.valueOf(values[3]),
                values[4]
        );
    }
}

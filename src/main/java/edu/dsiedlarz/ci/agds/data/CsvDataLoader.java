package edu.dsiedlarz.ci.agds.data;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CsvDataLoader implements DataLoader {
    @Override
    public List<GenericData> loadData(String fileName) {
        return new BufferedReader(new InputStreamReader(ClassLoader.getSystemResourceAsStream(fileName)))
                .lines().map(line -> {
                    String[] parts = line.split(",");
                    String className = parts[0];
                    Double[] attributes = Arrays.stream(Arrays.copyOfRange(parts, 1, parts.length)).map(Double::valueOf).toArray(Double[]::new);

                    return new GenericData(className, attributes);
                }).collect(Collectors.toList());
    }
}

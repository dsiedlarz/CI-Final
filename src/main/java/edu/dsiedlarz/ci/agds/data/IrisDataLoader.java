package edu.dsiedlarz.ci.agds.data;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class IrisDataLoader {

    String fileName;

    public IrisDataLoader(String fileName) {
        this.fileName = fileName;
    }

    public List<IrisData> loadData() {
        return new BufferedReader(new InputStreamReader(ClassLoader.getSystemResourceAsStream(fileName)))
                .lines().map(IrisData::ofLine).collect(Collectors.toList());

    }

    public static List<GenericData> normalize(List<GenericData> values) {

        List<GenericData> normalizedData = new ArrayList<>();
        final Double[] min = {Double.MAX_VALUE};
        final Double[] max = {Double.MIN_VALUE};


        values.forEach(singleValue -> {
            Stream.of(singleValue.getAttributes()).forEach(x -> {

                        if (x < min[0]) min[0] = x;
                        if (x > max[0]) max[0] = x;
                    }
            );
        });

        values.forEach(singleValue -> {
            normalizedData.add(new GenericData(
                    singleValue.getClassName(),
                    (singleValue.getAttributes()[0] - min[0]) / (max[0] - min[0]),
                    (singleValue.getAttributes()[1] - min[0]) / (max[0] - min[0]),
                    (singleValue.getAttributes()[2] - min[0]) / (max[0] - min[0]),
                    (singleValue.getAttributes()[3] - min[0]) / (max[0] - min[0])
            ));
        });

        return normalizedData;
    }
}

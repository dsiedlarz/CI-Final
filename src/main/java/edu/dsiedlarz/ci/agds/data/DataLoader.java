package edu.dsiedlarz.ci.agds.data;

import java.util.List;

public interface DataLoader {

    List<GenericData> loadData(String fileName);
}

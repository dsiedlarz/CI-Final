package edu.dsiedlarz.ci.agds.data;

public class GenericData {
    private final String className;
    private final Double[] attributes;

    public GenericData(String className, Double... attributes) {
        this.className = className;
        this.attributes = attributes;
    }

    public String getClassName() {
        return className;
    }

    public Double[] getAttributes() {
        return attributes;
    }
}

package edu.dsiedlarz.ci.agds.avb;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class AvbPlusTreeTest {

    @Test
    public void oneElement() {
        AvbPlusTree<Integer> tree = new AvbPlusTree<>();
        tree.insert(5);

        assertEquals(new Integer(5), tree.root.values.get(0).getValue());
        assertEquals(1, tree.root.values.get(0).counter);
    }

    @Test
    public void twoElements() {
        AvbPlusTree<Integer> tree = new AvbPlusTree<>();
        tree.insert(5);
        tree.insert(4);

        assertEquals(new Integer(4), tree.root.values.get(0).getValue());
        assertEquals(1, tree.root.values.get(0).counter);

        assertEquals(new Integer(5), tree.root.values.get(1).getValue());
        assertEquals(1, tree.root.values.get(1).counter);

        assertEquals(new Integer(5), tree.root.values.get(0).biggerNeighbour.getValue());
        assertEquals(new Integer(4), tree.root.values.get(1).smallerNeighbour.getValue());
    }

    @Test
    public void threeElements() {
        AvbPlusTree<Integer> tree = new AvbPlusTree<>();
        tree.insert(5);
        tree.insert(4);
        tree.insert(1);


        assertEquals(1, tree.root.values.size());

        assertEquals(new Integer(4), tree.root.values.get(0).getValue());
        assertEquals(1, tree.root.values.get(0).counter);

        assertEquals(new Integer(1), tree.root.children.get(0).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(0).values.get(0).counter);

        assertEquals(new Integer(5), tree.root.children.get(1).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(1).values.get(0).counter);

        assertEquals(new Integer(1), tree.root.values.get(0).smallerNeighbour.getValue());
        assertEquals(new Integer(5), tree.root.values.get(0).biggerNeighbour.getValue());
    }

    @Test
    public void fourElements() {
        AvbPlusTree<Integer> tree = new AvbPlusTree<>();
        tree.insert(5);
        tree.insert(4);
        tree.insert(1);
        tree.insert(9);


        assertEquals(1, tree.root.values.size());

        assertEquals(new Integer(4), tree.root.values.get(0).getValue());
        assertEquals(1, tree.root.values.get(0).counter);

        assertEquals(new Integer(1), tree.root.children.get(0).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(0).values.get(0).counter);

        assertEquals(new Integer(5), tree.root.children.get(1).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(1).values.get(0).counter);

        assertEquals(new Integer(9), tree.root.children.get(1).values.get(1).getValue());
        assertEquals(1, tree.root.children.get(1).values.get(1).counter);

        assertEquals(new Integer(1), tree.root.values.get(0).smallerNeighbour.getValue());
        assertEquals(new Integer(5), tree.root.values.get(0).biggerNeighbour.getValue());

        assertEquals(new Integer(9), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.getValue());
    }

    @Test
    public void fiveElements() {
        AvbPlusTree<Integer> tree = new AvbPlusTree<>();
        tree.insert(5);
        tree.insert(4);
        tree.insert(1);
        tree.insert(9);
        tree.insert(6);


        assertEquals(2, tree.root.values.size());

        assertEquals(new Integer(4), tree.root.values.get(0).getValue());
        assertEquals(1, tree.root.values.get(0).counter);

        assertEquals(new Integer(6), tree.root.values.get(1).getValue());
        assertEquals(1, tree.root.values.get(1).counter);

        assertEquals(new Integer(1), tree.root.children.get(0).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(0).values.get(0).counter);

        assertEquals(new Integer(5), tree.root.children.get(1).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(1).values.get(0).counter);

        assertEquals(new Integer(9), tree.root.children.get(2).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(2).values.get(0).counter);

        assertEquals(new Integer(1), tree.root.values.get(0).smallerNeighbour.getValue());
        assertEquals(new Integer(5), tree.root.values.get(0).biggerNeighbour.getValue());
        assertEquals(new Integer(6), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.getValue());

        assertEquals(new Integer(9), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.biggerNeighbour.getValue());
    }

    @Test
    public void sixElements() {
        AvbPlusTree<Integer> tree = new AvbPlusTree<>();
        tree.insert(5);
        tree.insert(4);
        tree.insert(1);
        tree.insert(9);
        tree.insert(6);
        tree.insert(3);


        assertEquals(2, tree.root.values.size());

        assertEquals(new Integer(4), tree.root.values.get(0).getValue());
        assertEquals(1, tree.root.values.get(0).counter);

        assertEquals(new Integer(6), tree.root.values.get(1).getValue());
        assertEquals(1, tree.root.values.get(1).counter);

        assertEquals(new Integer(1), tree.root.children.get(0).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(0).values.get(0).counter);

        assertEquals(new Integer(3), tree.root.children.get(0).values.get(1).getValue());
        assertEquals(1, tree.root.children.get(0).values.get(1).counter);

        assertEquals(new Integer(5), tree.root.children.get(1).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(1).values.get(0).counter);

        assertEquals(new Integer(9), tree.root.children.get(2).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(2).values.get(0).counter);

        assertEquals(new Integer(1), tree.root.values.get(0).smallerNeighbour.smallerNeighbour.getValue());
        assertEquals(new Integer(3), tree.root.values.get(0).smallerNeighbour.getValue());
        assertEquals(new Integer(5), tree.root.values.get(0).biggerNeighbour.getValue());
        assertEquals(new Integer(6), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.getValue());
        assertEquals(new Integer(9), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.biggerNeighbour.getValue());
    }

    @Test
    public void sevenElements() {
        AvbPlusTree<Integer> tree = new AvbPlusTree<>();
        tree.insert(5);
        tree.insert(4);
        tree.insert(1);
        tree.insert(9);
        tree.insert(6);
        tree.insert(3);
        tree.insert(6);


        assertEquals(2, tree.root.values.size());

        assertEquals(new Integer(4), tree.root.values.get(0).getValue());
        assertEquals(1, tree.root.values.get(0).counter);

        assertEquals(new Integer(6), tree.root.values.get(1).getValue());
        assertEquals(2, tree.root.values.get(1).counter);

        assertEquals(new Integer(1), tree.root.children.get(0).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(0).values.get(0).counter);

        assertEquals(new Integer(3), tree.root.children.get(0).values.get(1).getValue());
        assertEquals(1, tree.root.children.get(0).values.get(1).counter);

        assertEquals(new Integer(5), tree.root.children.get(1).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(1).values.get(0).counter);

        assertEquals(new Integer(9), tree.root.children.get(2).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(2).values.get(0).counter);

        assertEquals(new Integer(1), tree.root.values.get(0).smallerNeighbour.smallerNeighbour.getValue());
        assertEquals(new Integer(3), tree.root.values.get(0).smallerNeighbour.getValue());
        assertEquals(new Integer(5), tree.root.values.get(0).biggerNeighbour.getValue());
        assertEquals(new Integer(6), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.getValue());
        assertEquals(new Integer(9), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.biggerNeighbour.getValue());
    }

    @Test
    public void eightElements() {
        AvbPlusTree<Integer> tree = new AvbPlusTree<>();
        tree.insert(5);
        tree.insert(4);
        tree.insert(1);
        tree.insert(9);
        tree.insert(6);
        tree.insert(3);
        tree.insert(6);
        tree.insert(5);


        assertEquals(2, tree.root.values.size());

        assertEquals(new Integer(4), tree.root.values.get(0).getValue());
        assertEquals(1, tree.root.values.get(0).counter);

        assertEquals(new Integer(6), tree.root.values.get(1).getValue());
        assertEquals(2, tree.root.values.get(1).counter);

        assertEquals(new Integer(1), tree.root.children.get(0).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(0).values.get(0).counter);

        assertEquals(new Integer(3), tree.root.children.get(0).values.get(1).getValue());
        assertEquals(1, tree.root.children.get(0).values.get(1).counter);

        assertEquals(new Integer(5), tree.root.children.get(1).values.get(0).getValue());
        assertEquals(2, tree.root.children.get(1).values.get(0).counter);

        assertEquals(new Integer(9), tree.root.children.get(2).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(2).values.get(0).counter);

        assertEquals(new Integer(1), tree.root.values.get(0).smallerNeighbour.smallerNeighbour.getValue());
        assertEquals(new Integer(3), tree.root.values.get(0).smallerNeighbour.getValue());
        assertEquals(new Integer(5), tree.root.values.get(0).biggerNeighbour.getValue());
        assertEquals(new Integer(6), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.getValue());
        assertEquals(new Integer(9), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.biggerNeighbour.getValue());
    }


    @Test
    public void nineElements() {
        AvbPlusTree<Integer> tree = new AvbPlusTree<>();
        tree.insert(5);
        tree.insert(4);
        tree.insert(1);
        tree.insert(9);
        tree.insert(6);
        tree.insert(3);
        tree.insert(6);
        tree.insert(5);
        tree.insert(8);


        assertEquals(2, tree.root.values.size());

        assertEquals(new Integer(4), tree.root.values.get(0).getValue());
        assertEquals(1, tree.root.values.get(0).counter);

        assertEquals(new Integer(6), tree.root.values.get(1).getValue());
        assertEquals(2, tree.root.values.get(1).counter);

        assertEquals(new Integer(1), tree.root.children.get(0).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(0).values.get(0).counter);

        assertEquals(new Integer(3), tree.root.children.get(0).values.get(1).getValue());
        assertEquals(1, tree.root.children.get(0).values.get(1).counter);

        assertEquals(new Integer(5), tree.root.children.get(1).values.get(0).getValue());
        assertEquals(2, tree.root.children.get(1).values.get(0).counter);

        assertEquals(new Integer(8), tree.root.children.get(2).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(2).values.get(0).counter);

        assertEquals(new Integer(9), tree.root.children.get(2).values.get(1).getValue());
        assertEquals(1, tree.root.children.get(2).values.get(1).counter);

        assertEquals(new Integer(1), tree.root.values.get(0).smallerNeighbour.smallerNeighbour.getValue());
        assertEquals(new Integer(3), tree.root.values.get(0).smallerNeighbour.getValue());
        assertEquals(new Integer(5), tree.root.values.get(0).biggerNeighbour.getValue());
        assertEquals(new Integer(6), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.getValue());
        assertEquals(new Integer(8), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.biggerNeighbour.getValue());
        assertEquals(new Integer(9), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.biggerNeighbour.biggerNeighbour.getValue());
    }

    @Test
    public void tenElements() {
        AvbPlusTree<Integer> tree = new AvbPlusTree<>();
        tree.insert(5);
        tree.insert(4);
        tree.insert(1);
        tree.insert(9);
        tree.insert(6);
        tree.insert(3);
        tree.insert(6);
        tree.insert(5);
        tree.insert(8);
        tree.insert(3);


        assertEquals(2, tree.root.values.size());

        assertEquals(new Integer(4), tree.root.values.get(0).getValue());
        assertEquals(1, tree.root.values.get(0).counter);

        assertEquals(new Integer(6), tree.root.values.get(1).getValue());
        assertEquals(2, tree.root.values.get(1).counter);

        assertEquals(new Integer(1), tree.root.children.get(0).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(0).values.get(0).counter);

        assertEquals(new Integer(3), tree.root.children.get(0).values.get(1).getValue());
        assertEquals(2, tree.root.children.get(0).values.get(1).counter);

        assertEquals(new Integer(5), tree.root.children.get(1).values.get(0).getValue());
        assertEquals(2, tree.root.children.get(1).values.get(0).counter);

        assertEquals(new Integer(8), tree.root.children.get(2).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(2).values.get(0).counter);

        assertEquals(new Integer(9), tree.root.children.get(2).values.get(1).getValue());
        assertEquals(1, tree.root.children.get(2).values.get(1).counter);

        assertEquals(new Integer(1), tree.root.values.get(0).smallerNeighbour.smallerNeighbour.getValue());
        assertEquals(new Integer(3), tree.root.values.get(0).smallerNeighbour.getValue());
        assertEquals(new Integer(5), tree.root.values.get(0).biggerNeighbour.getValue());
        assertEquals(new Integer(6), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.getValue());
        assertEquals(new Integer(8), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.biggerNeighbour.getValue());
        assertEquals(new Integer(9), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.biggerNeighbour.biggerNeighbour.getValue());
    }

    @Test
    public void elevenElements() {
        AvbPlusTree<Integer> tree = new AvbPlusTree<>();
        tree.insert(5);
        tree.insert(4);
        tree.insert(1);
        tree.insert(9);
        tree.insert(6);
        tree.insert(3);
        tree.insert(6);
        tree.insert(5);
        tree.insert(8);
        tree.insert(3);
        tree.insert(1);


        assertEquals(2, tree.root.values.size());

        assertEquals(new Integer(4), tree.root.values.get(0).getValue());
        assertEquals(1, tree.root.values.get(0).counter);

        assertEquals(new Integer(6), tree.root.values.get(1).getValue());
        assertEquals(2, tree.root.values.get(1).counter);

        assertEquals(new Integer(1), tree.root.children.get(0).values.get(0).getValue());
        assertEquals(2, tree.root.children.get(0).values.get(0).counter);

        assertEquals(new Integer(3), tree.root.children.get(0).values.get(1).getValue());
        assertEquals(2, tree.root.children.get(0).values.get(1).counter);

        assertEquals(new Integer(5), tree.root.children.get(1).values.get(0).getValue());
        assertEquals(2, tree.root.children.get(1).values.get(0).counter);

        assertEquals(new Integer(8), tree.root.children.get(2).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(2).values.get(0).counter);

        assertEquals(new Integer(9), tree.root.children.get(2).values.get(1).getValue());
        assertEquals(1, tree.root.children.get(2).values.get(1).counter);

        assertEquals(new Integer(1), tree.root.values.get(0).smallerNeighbour.smallerNeighbour.getValue());
        assertEquals(new Integer(3), tree.root.values.get(0).smallerNeighbour.getValue());
        assertEquals(new Integer(5), tree.root.values.get(0).biggerNeighbour.getValue());
        assertEquals(new Integer(6), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.getValue());
        assertEquals(new Integer(8), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.biggerNeighbour.getValue());
        assertEquals(new Integer(9), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.biggerNeighbour.biggerNeighbour.getValue());
    }


    @Test
    public void twelveElements() {
        AvbPlusTree<Integer> tree = new AvbPlusTree<>();
        tree.insert(5);
        tree.insert(4);
        tree.insert(1);
        tree.insert(9);
        tree.insert(6);
        tree.insert(3);
        tree.insert(6);
        tree.insert(5);
        tree.insert(8);
        tree.insert(3);
        tree.insert(1);
        tree.insert(2);

        assertEquals(1, tree.root.values.size());

        assertEquals(new Integer(4), tree.root.values.get(0).getValue());
        assertEquals(1, tree.root.values.get(0).counter);


        assertEquals(new Integer(2), tree.root.children.get(0).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(0).values.get(0).counter);

        assertEquals(new Integer(6), tree.root.children.get(1).values.get(0).getValue());
        assertEquals(2, tree.root.children.get(1).values.get(0).counter);


        assertEquals(new Integer(1), tree.root.children.get(0).children.get(0).values.get(0).getValue());
        assertEquals(2, tree.root.children.get(0).children.get(0).values.get(0).counter);


        assertEquals(new Integer(3), tree.root.children.get(0).children.get(1).values.get(0).getValue());
        assertEquals(2, tree.root.children.get(0).children.get(1).values.get(0).counter);


        assertEquals(new Integer(5), tree.root.children.get(1).children.get(0).values.get(0).getValue());
        assertEquals(2, tree.root.children.get(1).children.get(0).values.get(0).counter);

        assertEquals(new Integer(8), tree.root.children.get(1).children.get(1).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(1).children.get(1).values.get(0).counter);

        assertEquals(new Integer(9), tree.root.children.get(1).children.get(1).values.get(1).getValue());
        assertEquals(1, tree.root.children.get(1).children.get(1).values.get(1).counter);

        assertEquals(new Integer(1), tree.root.values.get(0).smallerNeighbour.smallerNeighbour.smallerNeighbour.getValue());
        assertEquals(new Integer(2), tree.root.values.get(0).smallerNeighbour.smallerNeighbour.getValue());
        assertEquals(new Integer(3), tree.root.values.get(0).smallerNeighbour.getValue());
        assertEquals(new Integer(4), tree.root.values.get(0).biggerNeighbour.smallerNeighbour.getValue());
        assertEquals(new Integer(5), tree.root.values.get(0).biggerNeighbour.getValue());
        assertEquals(new Integer(6), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.getValue());
        assertEquals(new Integer(8), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.biggerNeighbour.getValue());
        assertEquals(new Integer(9), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.biggerNeighbour.biggerNeighbour.getValue());
    }

    @Test
    public void thirteenElements() {
        AvbPlusTree<Integer> tree = new AvbPlusTree<>();
        tree.insert(5);
        tree.insert(4);
        tree.insert(1);
        tree.insert(9);
        tree.insert(6);
        tree.insert(3);
        tree.insert(6);
        tree.insert(5);
        tree.insert(8);
        tree.insert(3);
        tree.insert(1);
        tree.insert(2);
        tree.insert(5);

        assertEquals(1, tree.root.values.size());

        assertEquals(new Integer(4), tree.root.values.get(0).getValue());
        assertEquals(1, tree.root.values.get(0).counter);


        assertEquals(new Integer(2), tree.root.children.get(0).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(0).values.get(0).counter);

        assertEquals(new Integer(6), tree.root.children.get(1).values.get(0).getValue());
        assertEquals(2, tree.root.children.get(1).values.get(0).counter);


        assertEquals(new Integer(1), tree.root.children.get(0).children.get(0).values.get(0).getValue());
        assertEquals(2, tree.root.children.get(0).children.get(0).values.get(0).counter);


        assertEquals(new Integer(3), tree.root.children.get(0).children.get(1).values.get(0).getValue());
        assertEquals(2, tree.root.children.get(0).children.get(1).values.get(0).counter);


        assertEquals(new Integer(5), tree.root.children.get(1).children.get(0).values.get(0).getValue());
        assertEquals(3, tree.root.children.get(1).children.get(0).values.get(0).counter);

        assertEquals(new Integer(8), tree.root.children.get(1).children.get(1).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(1).children.get(1).values.get(0).counter);

        assertEquals(new Integer(9), tree.root.children.get(1).children.get(1).values.get(1).getValue());
        assertEquals(1, tree.root.children.get(1).children.get(1).values.get(1).counter);

        assertEquals(new Integer(1), tree.root.values.get(0).smallerNeighbour.smallerNeighbour.smallerNeighbour.getValue());
        assertEquals(new Integer(2), tree.root.values.get(0).smallerNeighbour.smallerNeighbour.getValue());
        assertEquals(new Integer(3), tree.root.values.get(0).smallerNeighbour.getValue());
        assertEquals(new Integer(4), tree.root.values.get(0).biggerNeighbour.smallerNeighbour.getValue());
        assertEquals(new Integer(5), tree.root.values.get(0).biggerNeighbour.getValue());
        assertEquals(new Integer(6), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.getValue());
        assertEquals(new Integer(8), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.biggerNeighbour.getValue());
        assertEquals(new Integer(9), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.biggerNeighbour.biggerNeighbour.getValue());
    }

    @Test
    public void fourteenElements() {
        AvbPlusTree<Integer> tree = new AvbPlusTree<>();
        tree.insert(5);
        tree.insert(4);
        tree.insert(1);
        tree.insert(9);
        tree.insert(6);
        tree.insert(3);
        tree.insert(6);
        tree.insert(5);
        tree.insert(8);
        tree.insert(3);
        tree.insert(1);
        tree.insert(2);
        tree.insert(5);
        tree.insert(7);

        assertEquals(1, tree.root.values.size());

        assertEquals(new Integer(4), tree.root.values.get(0).getValue());
        assertEquals(1, tree.root.values.get(0).counter);


        assertEquals(new Integer(2), tree.root.children.get(0).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(0).values.get(0).counter);

        assertEquals(new Integer(6), tree.root.children.get(1).values.get(0).getValue());
        assertEquals(2, tree.root.children.get(1).values.get(0).counter);

        assertEquals(new Integer(8), tree.root.children.get(1).values.get(1).getValue());
        assertEquals(1, tree.root.children.get(1).values.get(1).counter);

        assertEquals(new Integer(1), tree.root.children.get(0).children.get(0).values.get(0).getValue());
        assertEquals(2, tree.root.children.get(0).children.get(0).values.get(0).counter);


        assertEquals(new Integer(3), tree.root.children.get(0).children.get(1).values.get(0).getValue());
        assertEquals(2, tree.root.children.get(0).children.get(1).values.get(0).counter);


        assertEquals(new Integer(5), tree.root.children.get(1).children.get(0).values.get(0).getValue());
        assertEquals(3, tree.root.children.get(1).children.get(0).values.get(0).counter);

        assertEquals(new Integer(7), tree.root.children.get(1).children.get(1).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(1).children.get(1).values.get(0).counter);

        assertEquals(new Integer(9), tree.root.children.get(1).children.get(2).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(1).children.get(2).values.get(0).counter);

        assertEquals(new Integer(1), tree.root.values.get(0).smallerNeighbour.smallerNeighbour.smallerNeighbour.getValue());
        assertEquals(new Integer(2), tree.root.values.get(0).smallerNeighbour.smallerNeighbour.getValue());
        assertEquals(new Integer(3), tree.root.values.get(0).smallerNeighbour.getValue());
        assertEquals(new Integer(4), tree.root.values.get(0).biggerNeighbour.smallerNeighbour.getValue());
        assertEquals(new Integer(5), tree.root.values.get(0).biggerNeighbour.getValue());
        assertEquals(new Integer(6), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.getValue());
        assertEquals(new Integer(7), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.biggerNeighbour.getValue());
        assertEquals(new Integer(8), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.biggerNeighbour.biggerNeighbour.getValue());
        assertEquals(new Integer(9), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.biggerNeighbour.biggerNeighbour.biggerNeighbour.getValue());
    }

    @Test
    public void fourteenElementsInDiffrentOrder() {
        AvbPlusTree<Integer> tree = new AvbPlusTree<>();
        tree.insert(1);
        tree.insert(1);
        tree.insert(2);
        tree.insert(3);
        tree.insert(3);
        tree.insert(4);
        tree.insert(5);
        tree.insert(5);
        tree.insert(5);
        tree.insert(6);
        tree.insert(6);
        tree.insert(7);
        tree.insert(8);
        tree.insert(9);

        assertEquals(1, tree.root.values.size());

        assertEquals(new Integer(4), tree.root.values.get(0).getValue());
        assertEquals(1, tree.root.values.get(0).counter);


        assertEquals(new Integer(2), tree.root.children.get(0).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(0).values.get(0).counter);

        assertEquals(new Integer(6), tree.root.children.get(1).values.get(0).getValue());
        assertEquals(2, tree.root.children.get(1).values.get(0).counter);

        assertEquals(new Integer(8), tree.root.children.get(1).values.get(1).getValue());
        assertEquals(1, tree.root.children.get(1).values.get(1).counter);

        assertEquals(new Integer(1), tree.root.children.get(0).children.get(0).values.get(0).getValue());
        assertEquals(2, tree.root.children.get(0).children.get(0).values.get(0).counter);


        assertEquals(new Integer(3), tree.root.children.get(0).children.get(1).values.get(0).getValue());
        assertEquals(2, tree.root.children.get(0).children.get(1).values.get(0).counter);


        assertEquals(new Integer(5), tree.root.children.get(1).children.get(0).values.get(0).getValue());
        assertEquals(3, tree.root.children.get(1).children.get(0).values.get(0).counter);

        assertEquals(new Integer(7), tree.root.children.get(1).children.get(1).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(1).children.get(1).values.get(0).counter);

        assertEquals(new Integer(9), tree.root.children.get(1).children.get(2).values.get(0).getValue());
        assertEquals(1, tree.root.children.get(1).children.get(2).values.get(0).counter);

        assertEquals(new Integer(1), tree.root.values.get(0).smallerNeighbour.smallerNeighbour.smallerNeighbour.getValue());
        assertEquals(new Integer(2), tree.root.values.get(0).smallerNeighbour.smallerNeighbour.getValue());
        assertEquals(new Integer(3), tree.root.values.get(0).smallerNeighbour.getValue());
        assertEquals(new Integer(4), tree.root.values.get(0).biggerNeighbour.smallerNeighbour.getValue());
        assertEquals(new Integer(5), tree.root.values.get(0).biggerNeighbour.getValue());
        assertEquals(new Integer(6), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.getValue());
        assertEquals(new Integer(7), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.biggerNeighbour.getValue());
        assertEquals(new Integer(8), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.biggerNeighbour.biggerNeighbour.getValue());
        assertEquals(new Integer(9), tree.root.values.get(0).biggerNeighbour.biggerNeighbour.biggerNeighbour.biggerNeighbour.biggerNeighbour.getValue());
    }


    @Test
    public void checkReturnedValues() {
        AvbPlusTree<Integer> tree = new AvbPlusTree<>();
        AvbPlusValue<Integer> value;

        value = tree.insert(1);
        assertEquals(Integer.valueOf(1), value.getValue());
        assertEquals(1, value.getCounter());

        value = tree.insert(1);
        assertEquals(Integer.valueOf(1), value.getValue());
        assertEquals(2, value.getCounter());

        value = tree.insert(2);
        assertEquals(Integer.valueOf(2), value.getValue());
        assertEquals(1, value.getCounter());

        value = tree.insert(3);
        assertEquals(Integer.valueOf(3), value.getValue());
        assertEquals(1, value.getCounter());

        value = tree.insert(3);
        assertEquals(Integer.valueOf(3), value.getValue());
        assertEquals(2, value.getCounter());

        value = tree.insert(4);
        assertEquals(Integer.valueOf(4), value.getValue());
        assertEquals(1, value.getCounter());

        value = tree.insert(5);
        assertEquals(Integer.valueOf(5), value.getValue());
        assertEquals(1, value.getCounter());


        value = tree.insert(5);
        assertEquals(Integer.valueOf(5), value.getValue());
        assertEquals(2, value.getCounter());

        value = tree.insert(5);
        assertEquals(Integer.valueOf(5), value.getValue());
        assertEquals(3, value.getCounter());

        value = tree.insert(6);
        assertEquals(Integer.valueOf(6), value.getValue());
        assertEquals(1, value.getCounter());

        value = tree.insert(6);
        assertEquals(Integer.valueOf(6), value.getValue());
        assertEquals(2, value.getCounter());

        value = tree.insert(7);
        assertEquals(Integer.valueOf(7), value.getValue());
        assertEquals(1, value.getCounter());

        value = tree.insert(8);
        assertEquals(Integer.valueOf(8), value.getValue());
        assertEquals(1, value.getCounter());


        value = tree.insert(9);
        assertEquals(Integer.valueOf(9), value.getValue());
        assertEquals(1, value.getCounter());
    }


    @Test
    public void testFindClosestValue() {
        AvbPlusTree<Double> tree = new AvbPlusTree<>();
        tree.insert(0.5D);

        AvbPlusValue<Double> value;

        value = tree.findClosestValue(0.3);
        assertEquals(Double.valueOf(0.5D), value.getValue());

        value = tree.findClosestValue(0.8);
        assertEquals(Double.valueOf(0.5D), value.getValue());


        tree.insert(0.8D);
        tree.insert(1.0D);
        tree.insert(4.0D);
        tree.insert(4.3D);


        value = tree.findClosestValue(4.14);
        assertEquals(Double.valueOf(4.0D), value.getValue());


        value = tree.findClosestValue(1.3);
        assertEquals(Double.valueOf(1.0D), value.getValue());


    }
}

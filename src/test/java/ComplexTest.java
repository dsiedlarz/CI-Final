import edu.dsiedlarz.ci.agds.agds.AGDS;
import edu.dsiedlarz.ci.agds.agds.AgdsElement;
import edu.dsiedlarz.ci.agds.avb.AvbPlusTree;
import edu.dsiedlarz.ci.agds.avb.AvbPlusValue;
import edu.dsiedlarz.ci.agds.data.IrisData;
import edu.dsiedlarz.ci.agds.data.IrisDataLoader;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

public class ComplexTest {

    @Test
    public void testIrisData() {

        IrisDataLoader irisDataLoader = new IrisDataLoader("Iris3.csv");
        List<IrisData> irisDataList = irisDataLoader.loadData();

        AGDS agds = new AGDS();

        int counter = 0;
        for (int i = 0; i < irisDataList.size(); i++) {
            IrisData irisData = irisDataList.get(i);
            if (irisData.values.get(0).compareTo(6.4D) == 0) {
                System.out.println("before:");
                System.out.println(agds.attributes.get(0).getAllValues().stream().map(el -> el.getValue().toString()).collect(Collectors.joining(" ")));

                counter++;
                if (counter == 2) {
                    System.out.println("prrr");
                }
            }


            AgdsElement element = agds.put("R" + i, irisData.name, irisData.values.toArray(new Double[irisData.values.size()]));
            if (irisData.values.get(0).compareTo(6.4D) == 0) {
                System.out.println("before:");
                System.out.println(agds.attributes.get(0).getAllValues().stream().map(el -> el.getValue().toString()).collect(Collectors.joining(" ")));
                System.out.println("\n\n\n\n");
            }
        }

        agds.attributes.stream().forEach(avbPlusTree -> {

            AvbPlusValue<Double> v = avbPlusTree.getMin();

            while (v != null) {

                if (v.getSmallerNeighbour() != null) {


                    System.out.println(v.getSmallerNeighbour().getValue() + " < " + v.getValue());
                    assertTrue(v.getValue().compareTo(v.getSmallerNeighbour().getValue()) > 0);
                }

                if (v.getBiggerNeighbour() != null) {
                    System.out.println(v.getValue().compareTo(v.getBiggerNeighbour().getValue()));
                    System.out.println(v.getValue() + " < " + v.getBiggerNeighbour().getValue());
                    assertTrue(v.getValue().compareTo(v.getBiggerNeighbour().getValue()) < 0);
                }

                v = v.getBiggerNeighbour();
            }

        });
    }


    @Test
    public void iris8test() {
        AvbPlusTree<Double> avbPlusTree = new AvbPlusTree<>();
        avbPlusTree.insert(5.1D);
        avbPlusTree.insert(6.5D);
        avbPlusTree.insert(7.0D);
        avbPlusTree.insert(7.2D);
        avbPlusTree.insert(4.8D);
        avbPlusTree.insert(5.1D);
        avbPlusTree.insert(5.0D);
        avbPlusTree.insert(5.5D);
        avbPlusTree.insert(6.4D);
    }
}

To run:

On unix systems

`./gradlew bootRun`

On windows

`gradlew.bat bootRun`

Application will be available at <a href="http://127.0.0.1:8080"> http://127.0.0.1:8080 </a>